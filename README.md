# survey
```
* 碳佐顧客意見表專案(web、touch devices)
* 資料來源：微管雲 - 線上問卷系統
```

### JavaScript framework
Vue CLI (Vue 2.0)
```
# Installation
yarn global add @vue/cli

# Creating a Project
vue create survey

# Buefy component library use Vue.js
```


### Css framework
Bulma & Tailwindcss(Preprocessor stylesheet language: Stylus)
```
vue add tailwind
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
