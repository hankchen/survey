import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [{
		path: "/",			name: "Home",
		component: () => import("../views/Home.vue"),
	}, {
		path: "/end",		name: "End",
		component: () => import("../views/End.vue"),
	}, {
		// path: "/s/:id",
		path: "/form",
		name: "FORM",
		component: () => import('../views/Form.vue')
	}, {
		path: '/Account',
		name: 'account',
		component: () => import('../views/Account.vue')
	}, {
		path: '/404',
		name: '404',
		component: () => import('../views/Page404.vue')
	}, {
		path: '*',
		redirect: '/404'
	}
];

const router = new VueRouter({
  // mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
