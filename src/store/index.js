// vue basic lib
import Vue from 'vue'
import Vuex from 'vuex'
import VueCookies from 'vue-cookies'
import axios from 'axios'

Vue.use(Vuex)
Vue.use(VueCookies)

// 客製/第三方 lib
// import md5 from 'md5'
import fetchData from 'src/libs/fetchData.js'
import router1 from 'src/router'

// import { deviseFunction, ensureDeviseSynced, deviseSetLoginInfo } from '../libs/deviseHelper.js'

import { showAlert, setLoading } from '../libs/appHelper.js'
// import { showAlert } from '../libs/appHelper.js'
import { S_Obj,L_Obj,IsMobile,IsFn, EncodeMe, HasIt, GroupBy } from 'src/fun1.js'	// 共用fun在此!
// import { S_Obj,L_Obj,IsMobile,IsFn, EncodeMe, GroupBy } from 'src/fun1.js'	// 共用fun在此!


// 依據環境參數，來決定 API baseUrl 的值
const Test8012 	= '8012test.jh8.tw'
	,AccData		= L_Obj.read('userData') || {}
	,HoHttp			= AccData.HoHttp || 'https://'+baseUrl
	,IsDev 			= S_Obj.isDebug 	//開發debug
	,IsStaging 	= S_Obj.isStaging
	,baseUrl 		= IsStaging ? Test8012 : '9001.jh8.tw'
	// ,IsHank 		= /hank/.test(AccData.UserCode)		// @@test

let IsTest	= false
let SubEnterpriseID	= ''

const state = {
  appSite: 'survey', //'kakar', // 系統參數: 站台
  promise: null, // 保存promise
  // 是否在殼裡面(判斷能否使用殼相關接口的依據)
  isDeviseApp: false,
  // 啟動/關閉 『讀取中』的動畫效果
  isLoading: false,
  loadingMsg: '',
  // 顯示手動輸入 QRcode 表格
  showInputQRCode: false,

  // CSS 全域參數(用於判斷 CSS 是否設定 -webkit-overflow-scrolling: touch)
  // true => touch,
  // false => auto (幾乎只用於首頁)
  cssTouch: true,
	searchURL:{},
  isPageName: '', //內頁名稱
  // API url
  api: {
    main: `https://${baseUrl}/public/AppData.ashx`,
    Url8012: `https://${Test8012}/public/AppDataVIP.ashx`,
    figUrl: `https://${baseUrl}/public/AppDataVIPOneEntry.ashx`,
    // OneEntry 主服務
    appNcw: `https://${baseUrl}/Public/AppNCWOneEntry.ashx`,
    // 企業的公開資料
    publicUrl: `https://${baseUrl}/public/newsocket.ashx`,
    // 企業的會員資料
    memberUrl: `https://${baseUrl}/public/AppDataVIP.ashx`,	// +token
    // 預約資料
    bookUrl: `https://${baseUrl}/public/AppBooking.ashx`,
    // 預約的token資料
    questUrl: `https://${baseUrl}/Public/question.ashx`,
    // 圖片的路徑
    picUrl: `https://${baseUrl}`,
  },
  // 基本資訊
  baseInfo: {
    // 英文常稱
    isFrom: 'surveyapp',
    // 企業號
    EnterpriseID: '6082824697',
    // FunctionID
    FunctionID: '450801', // 碳佐functionid=450801
    // 是否接收推播訊息 (跟殼要)
    pushNotify: '1', // 預設要開啟, 1:開啟推播, 0:關閉推播
    // GPS 資訊 (跟殼要)
    gps: {},
    // 螢幕亮度值 (跟殼要, range 0 ~ 255 預設為中間值)
    brightness: '120',
    // 前端打包檔版本號 (跟殼要)
    WebVer: '',
    // 殼的裝置 (iOS / Android)
    AppOS: '',
    // 殼是否有新版本
    DeviseVersionIsDiff: false,
  },
	// 會員登入頁的表單
	loginForm: { account: '', password: '' },
  // 簡訊認證設定 (需要從殼取)
  SMS_Config: {
    // 簡訊回傳的驗證碼
    reCode: "",
    // 每天最多簡訊次數 (預設為 5 次)
    APPSMSDayTimes: '5',
    // 每封簡訊發送間隔 (預設為 50 秒)
    AppSMSIntervalMin: '50',
    // 每日簡訊發送扣打 (用來紀錄已寄出幾封, 不得超過 APPSMSDayTimes 的值), 格式： yyyymmdd-times => e.g. 20190513-4 (05/13 已用四次)
    SMSSendQuota: '',
  },
  // 會員接口相關資訊
  member: {
    mac: AccData.mac,
    Tokenkey: AccData.tokenkey,
    code: AccData.UserCode,
  },
  // 會員帳號資訊
  userData: AccData,
  // 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠券 ... etc)
	// memberData: {
		// // 會員卡
		// card_info: {},
		// // 會員資料
		// vip_info: {},
		// // 會員條碼 QRcode
		// qrCode: { image: '', value: '', loaded: false, },
		// // 會員儲值交易資料(使用紀錄)
		// depositRecord: [],
	// },
  // 企業公開資料相關
  publicData: {
    // DB connect 位置
    connect: '9001',
    // 門店資訊頁
    storeList: { store: [] },
    // appDeliver: [], // 運銷商
  },
	arrEntGID: [{
			gid: '910654bb-47be-452c-887c-d0645bbc8dd0',
			key: "TZ",
			epid: "53855259",
			title: "碳佐麻里精品燒肉",
			text: "碳佐麻里",
			memo: "致 親愛的貴賓，期以為您獻上更臻完美的用餐體驗，請提供我們您的真實想法，我們將誠心誠意接受並立即改善及精進。若您對於本次服務感到滿意，我們也很期待得到您的鼓勵。您的滿意是我們最大的動力。<br><br>※ 我們每月將從中抽出30位幸運貴賓，贈送500元兌餐贈品券。請關注碳佐麻里精品燒肉-非吃不可粉絲團",
		}, {
			gid: '77579b94-b3e5-48ec-8ec7-81cffcdc8da6',
			key: "TG",
			epid: "42819072",
			title: "碳佐TANGO麻辣鴛鴦",
			memo: "致 親愛的貴賓，期以為您獻上更臻完美的用餐體驗，請提供我們您的真實想法，我們將誠心誠意接受並立即改善及精進。若您對於本次服務感到滿意，我們也很期待得到您的鼓勵。您的滿意是我們最大的動力。<br><br>※ 我們每月將從中抽出10位幸運貴賓，贈送500元兌餐贈品券。請關注碳佐TANGO麻辣鴛鴦官方粉絲團",
			text: "天鍋",
		}, {
			gid: '3f893a01-b176-46ba-9d46-a175d71172b9',
			key: "GS",
			epid: "64922255",
			title: "有你真好．火鍋沙龍",
			memo: "致 親愛的貴賓，期以為您獻上更臻完美的用餐體驗，請提供我們您的真實想法，我們將誠心誠意接受並立即改善及精進。若您對於本次服務感到滿意，我們也很期待得到您的鼓勵。您的滿意是我們最大的動力。",
			text: "有你真好-火鍋沙龍",
		}, {
			gid: '8de25df8-6646-4f71-b066-a12d9c94c3a9',
			key: "HC",
			epid: "64922255",
			title: "有你真好．湘菜沙龍",
			memo: "致 親愛的貴賓，期以為您獻上更臻完美的用餐體驗，請提供我們您的真實想法，我們將誠心誠意接受並立即改善及精進。若您對於本次服務感到滿意，我們也很期待得到您的鼓勵。您的滿意是我們最大的動力。",
			text: "有你真好-湘菜沙龍",
		}
	],
  questInfo: {},

  // 卡片條碼 QRcode
  cardQRCode: { image: '', value: '', loaded: false },
  cardListScroll2: 0,
  fontSizeBase: 1
}

const mutations = {
  /** 暫存字體大小 */
  setFontSizeTemp(state, fontSize) {
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    let floatFontSize = parseFloat(fontSize)
    state.fontSizeBase = floatFontSize
  },
  /** 永久保存字體大小 */
  setFontSizeForever(state, fontSize) {
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    let floatFontSize = parseFloat(fontSize)
    state.fontSizeBase = floatFontSize
    // 存入cookie
    VueCookies.set('fontSize', floatFontSize)
    // 若在殼中 => 存入殼裡
    if (IsMobile()) {
      // let setThing = `{"spName":"fontSize", "spValue": ${ floatFontSize }}`
      // deviseFunction('SetSP', setThing, '')
      // console.log('===> 已存入殼裡 =>' + setThing)
    }
  },
  // 設定 cssTouch
  setCssTouch(state, status){ state.cssTouch = status },
  // 設定是否在殼裡面
  setIsDeviseApp(state, status) { state.isDeviseApp = status },
  // 『讀取中...』 切換
  setLoading(state, data) {
    state.isLoading = data.status
  },
  // 『顯示手動輸入 QRcode 表格』切換
  setInputQRCode(state, status) { state.showInputQRCode = status },

  // 存入手機簡訊認證的設定
  setSMS_Config(state, data) { state.SMS_Config = Object.assign({}, state.SMS_Config, data) },
  // 設定簡訊驗證碼
  setSMSreCode(state, data) { state.SMS_Config.reCode = data },
  // 存入基本資訊
  setBaseInfo(state, data) { state.baseInfo = Object.assign({}, state.baseInfo, data) },
  // 更新會員資訊
  // setUserData(state, data) { state.userData = data },
  // 存入Mac (在沒有登入的狀態)
  // setMacNoLogin(state, mac) {
    // // 存入 store
    // state.member.mac = mac
  // },
  // 存入會員登入資訊
	// setLoginInfo(state, payload) {
		// const data = payload.data

		// // 存入 store
		// state.userData = data
		// state.member.mac = data.mac
		// state.member.code = data.UserCode
	// },

  // 存入-子企業號
	// setSubEnterpriseID(v1) { SubEnterpriseID = v1 },
	setSubEnterpriseID(state, v1) {
			console.log('setSubEnterpriseID-v1: ', v1);	// @@

		SubEnterpriseID = v1
	},
  // 存入-問卷資訊
	setQuestiondata(state, data) { state.questInfo = data },

  // 存入條碼 QRcode
	setShowQrcode(state, data) { state.ShowQrcodeFn = data },
  // 存入殼的版本是否需要更新
  SetDeviseVersionIsDiff(state, status) {
    const value = (status === '1') ? true : false
    state.baseInfo.DeviseVersionIsDiff = value
  },
  /* 存入 運銷商 */
  // setDeliver(state, data) {
    // state.publicData.appDeliver = data || [];
  // },

}

const getters = {
  fontSizeBase: state => {
    let fontSize = state.fontSizeBase
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    return parseFloat(fontSize)
  },
  // 站台 router
  router: () => {//state => {
    // if (state.appSite === 'wuhulife') return routerWuhulife
    return router1
  },
	requestHead: () => {
		return { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
	},
  // 的 requestBody
  kakarBody: state => {
    const {mac, code} = state.member
    const {isFrom, EnterpriseID, FunctionID} = state.baseInfo
    return { FunctionID, EnterpriseID, isFrom, mac, Account: code, }
  },
  // 企業 app 的 requestBody
  appBody: state => (body) => {
    const inputBody = ( body || {} )
    const {mac} = state.member
    let {isFrom, EnterpriseID} = state.baseInfo
		const epid = state.userData.EnterPriseID
		if (epid) {
			EnterpriseID = epid;
		} else {
			if (state.member.mac) {
				throw 'No EnterpriseID!!';
			}
		}

    const basicBody = {
      mac, isFrom, EnterpriseID,
      // EnterpriseID: EnterPriseID,
      Account: state.member.code,
    }
    return Object.assign({}, basicBody, inputBody)
  },
  connect: (state) => {
    return state.publicData.connect
  },
  // 取手機的螢幕亮度值
  deviseBrightness: state => {
    if (state.baseInfo.brightness <= 70) return 120
    return state.baseInfo.brightness
  },
}

const actions = {
  /* 資料相關 */
  // init 時執行所需的 action
  // async fetchInitData() {
    // // 1. 跟殼取資料
    // await ensureDeviseSynced()
  // },

	/* 短網址-加密 */
  async calcuSurl({state,commit}, formData) {
    formData.qrType = formData.qrType || "2";
    formData.exType = formData.exType || "1";
    formData.data = formData.data || "{}";
    setLoading(true)
    const url = "https://surl.jh8.tw/AppShortURL.ashx"
		const {EnterpriseID} = state.baseInfo
		var body = { act: "SetUrl",
									EnterPriseID: EnterpriseID,
									BackData: formData.data,
									QrCodeType: formData.qrType,//1:url直接轉址,2:只運行 BackData
									ExpiredType: formData.exType //1:限1次,0:無限,2:限3天
								};
    if (formData.url) body.BackUrl = formData.url;
		const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
		const {data} = await axios.post( url, body, head )
		commit('setLoading', false)
		if (data.ErrorCode != '0') showAlert(data.ErrorMsg || "")
    if (IsFn(formData.fn)) formData.fn((data.Remark || ''));
		return (data.Remark || '');
  },
	/* 短網址-解密 */
  async goSurl({commit}, formData) {
    //限只會back data的jinher surl
		setLoading(true)
		await axios.post( `https://surl.jh8.tw/${formData.sUrl}`)
		.then((res) => {
			commit('setLoading', false);
      IsFn(formData.fn) && formData.fn(res.data);

		}).catch( () => {
      IsFn(formData.fn) && formData.fn({});
			commit('setLoading', false)
		})

	},

  async fetchQuestion({state, commit, getters}, payload){
    const url = state.api.questUrl
    let body = getters.appBody({ act: "getquestion", Question_GID: payload.gid,EnterpriseID: SubEnterpriseID })
		// body.EnterPriseID = 
    let data = await fetchData({url, body})
    // console.log('fetchQuestion-data: ', data);	// @@
    if (data.error) return

    // console.log('fetchQuestion-SortBy: ', SortBy);	// @@
		// data = SortBy.call(data, 'SequenceT');

/** @Debug: 待查用 */
if (IsDev && data.length) {
	let tmparr2 = []	// @@
		data.forEach((op1)=>{
			let {SequenceT,Question_Name,SequenceD,Item_Name,ItemType,SequenceI,ChoiceName,ChoiceValue} = op1
			tmparr2.push({SequenceT,Question_Name,SequenceD,Item_Name,ItemType,SequenceI,ChoiceName,ChoiceValue});
		})
	console.table(tmparr2)	// @@
}
		// console.warn('SortBy-data: ', data);	// @@
		// const arrPage 	= GroupBy(data, "SequenceT");
		// console.log('fetchQuestion-arrPage: ', arrPage);	// @@

		const nLen = data.length
		let i = 0, arrList = [];
		while (i<nLen) {
			const	o1 	= data[i]
				,v1 		= o1.SequenceT
				,source1 = data.filter(o => o.SequenceT == v1)

			if (!HasIt.call(arrList, 'SequenceT', v1)) {
				let arr2 = [];
				source1.forEach((op1)=>{
					if (op1.SequenceT == v1) {
						let {GID,SequenceD,Item_Name,ItemType,ChoiceName,ChoiceValue,Item_GID,ChoiceItem_GID} = op1
						arr2.push({GID,SequenceD,Item_Name,ItemType,ChoiceName,ChoiceValue,Item_GID,ChoiceItem_GID});
						// arr2.push(op1);		// @@
					}
					// console.error('fetchQuestion-arr2: ', arr2); // @@
				})
				o1.arrRes = arr2;
				// console.error('fetchQuestion-o1: ', o1); // @@
				arrList.push(o1);
			}
			i++;
		}

		arrList.forEach((op2)=>{
			op2.arrAns = GroupBy(op2.arrRes, "Item_GID");
			op2.arrWer = [];	// 存答案用
			delete op2.arrRes;
		})
		IsDev && console.error('fetchQuestion-arrList: ', arrList); // @@


		S_Obj.save('QuestInfo', arrList);
    commit('setQuestiondata', arrList);
  },

  // async fetchDeliver({getters, commit}) {
    // const data = await Get950201Body(getters, 'M_Scales_Deliver')
    // data && commit('setDeliver', data)
  // },
	/* 填完問卷 寫入-表頭記錄 */
  async ensureQuestionOrders({getters}, payload){
    const url = state.api.questUrl
    let body = getters.appBody({ act: "setquestionorders", EnterPriseID: SubEnterpriseID, UPdata: payload})
		// delete body.EnterpriseID;

    // console.log('表頭記錄-body: ', body);	// @@
    let data = await fetchData({url, body})
    // console.log('表頭記錄-data: ', data);	// @@
		return data;
		// const isOK = data.ErrorCode == 0
		// if (isOK) {
			// return (data || {});
		// } else {
			// return showAlert(data.ErrorMsg)
		// }
  },
	/* 填完問卷 寫入-表身記錄 */
  async ensureQuestionMember({getters}, payload){
    const url = state.api.questUrl
    let body = getters.appBody({ act: "setquestionmember", EnterPriseID: SubEnterpriseID, UPdata: payload.arr1})
    // console.log('表身記錄-body: ', body);	// @@

    let data = await fetchData({url, body})
    // console.log('fetchQuestion-data: ', data);	// @@
		const isOK = data.ErrorCode == 0
		if (!isOK) showAlert(data.ErrorMsg)
    return isOK
  },
	/* gid抓/作廢-秤重記錄(掃碼 */
  async GetSetScaleGid({getters}, payload) {
		console.log('秤重記錄-IsTest, IsDev: ', IsTest, IsDev);	// @@

		let bWrite 	= 0
			,gid2			= ''
			,lotNo		= ''

		if (payload) {
			bWrite = 1
			gid2 = payload.GID
			// lotNo = payload.LotNo
		} else {
			gid2 = S_Obj.read('M_Scales_GID');
			lotNo = S_Obj.read('M_Scales_LotNo');
		}

		const param1 = {
			GID: gid2,
			LotNo: lotNo,
			DeleFlag: bWrite
		};

    const data = await Get950201Body(getters, 'M_Scales_DeleFlag', param1)

// IsHank && alert('掃碼_秤重記錄-param1: '+JSON.stringify(param1)+'\ndata: \n'+JSON.stringify(data));	// @@

		// console.warn('GetSetScaleGid-data: ', data);	// @@
		if (!bWrite) {	// 掃碼
			if (data.length) {
				S_Obj.save('M_SCALES_ONE', data[0]);
				location.hash = '#/weigscan'

			} else {
				showAlert("找不到記錄: "+lotNo);
			}
		// } else {		// 作廢
		}

  },

  //  取 cardQRcode
  async fetchCardQRCodeData({state, commit, getters}, GID) {
    // 先清空舊的 QRcode
    commit('setCardQRCodeData', { value: '', image: '', loaded: false } )
    const url = state.api.memberUrl
    const body = getters.appBody({act: 'GetVipCardQrCodeStr', GID})
    const data = await fetchData({url, body})
    if (data.error) return
    commit('setCardQRCodeData', { value: data.QrCode, image: data.QrCodeImg, loaded: true } )

  },

  async fetchAppConfig({state, commit},str_keys) {
    const url = `https://${baseUrl}/public/GetAppSysConfigOneEntry.ashx?EnterpriseID=${state.baseInfo.EnterpriseID}&AppSysName=${str_keys}`
      ,body   ={}
      ,data 	= await fetchData({url, body})

    if (data && typeof data == 'string' && data.indexOf('OK,') == 0){
      const arr = data.split('OK,');
      if (arr.length > 1){
        const p_txt = `{${arr[1]}}`.replace(/'/g, "\"")
        const p_config = JSON.parse(p_txt);
        //str_keys有可能為 foodMarketShopID,xxx,yyy...不能直接p_config[str_keys]
        commit('setMarketShopID', p_config.foodMarketShopID)
      }
    }
  },

}

/* comm fun共用區 */
// async function Get950201Body(getters, _act, para1) {
async function Get950201Body(getters, _act, para1) {
	let param7 = {act: _act,FunctionID:"950201"}
	if (para1) {
		if (para1.orderby) {
			param7.orderby = para1.orderby;
			delete para1.orderby;
		}
		if (para1.srow) {
			param7.srow = para1.srow;
			delete para1.srow;
		}
		if (para1.erow) {
			param7.erow = para1.erow;
			delete para1.erow;
		}
		param7.param = EncodeMe(para1);
	}
	let body = getters.appBody(param7)

	const url = GetCUrl(state.api.main)
		,data 	= await fetchData({url, body})
	// return data.error ? null : data
	return data
}

function GetCUrl(curUrl) {
	const npos  = curUrl.lastIndexOf("/public")
		,_code = (npos != -1) ? curUrl.substring(npos) : ''
		,_url		= HoHttp+_code
	// console.log('GetCUrl-_url: ', _url);	// @@
	return _url;
}

export default new Vuex.Store({ state, mutations, getters, actions })