// import axios from 'axios'
// import store from 'src/store'
// import { S_Obj, L_Obj } from 'src/s_obj.js'
import { S_Obj } from 'src/s_obj.js'


let This1 = S_Obj.read('SetThis1') || {}		// 畫面用的 curOne

function ShowTheme() {
	const ke1 = This1.key
	if (ke1) {
		return 'theme-'+ke1.toLocaleLowerCase();
	}
	return '';
}
function SetThis1(one) {
	This1 = one;
	S_Obj.save('SetThis1', one);
}
// function GetThis1() {
	// return This1;
// }






export {
	ShowTheme, SetThis1, This1
}